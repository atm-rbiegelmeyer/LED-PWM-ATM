#include <Arduino.h>
#include <TimerOne.h>

#define LED1 11
#define LED2 10

int ledC1 = 255;
int ledC2 = 255;

void setup()
{ 
    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
    initialize(1);
    
}

void loop()
{
    //Exemplo tosco mesmo
    analogWrite(LED1, 128); 
    analogWrite(LED2, 0); 
    delay(10000);
    analogWrite(LED1, 255); 
    analogWrite(LED2, 200); 
    delay(10000);
}